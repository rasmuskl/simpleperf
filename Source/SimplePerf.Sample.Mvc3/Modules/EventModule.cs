﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using SimplePerf.Util;

namespace SimplePerf.Sample.Mvc3.Modules
{
    public class EventModule : IHttpModule
    {
        public static readonly EventStream EventStream = new EventStream("EventModule");

        public void Init(HttpApplication context)
        {
            context.BeginRequest += ContextOnBeginRequest;
            context.EndRequest += ContextOnEndRequest;
            context.AuthenticateRequest += ContextOnAuthenticateRequest;
            context.AuthorizeRequest += ContextOnAuthorizeRequest;
            context.LogRequest += ContextOnLogRequest;
            context.MapRequestHandler += ContextOnMapRequestHandler;
            context.PreRequestHandlerExecute += ContextOnPreRequestHandlerExecute;
            context.PostRequestHandlerExecute += ContextOnPostRequestHandlerExecute;
        }

        private void ContextOnPostRequestHandlerExecute(object sender, EventArgs e)
        {
            EventStream.AddEvent(HttpContext.Current.Request.Path + " - PostRequestHandlerExecute");
        }

        private void ContextOnPreRequestHandlerExecute(object sender, EventArgs eventArgs)
        {
            EventStream.AddEvent(HttpContext.Current.Request.Path + " - PreRequestHandlerExecute");
            IHttpHandler currentHandler = HttpContext.Current.CurrentHandler;

            var page = currentHandler as Page;

            if(page != null)
            {
                EventStream.AddEvent(currentHandler.GetType()+" is a WebForms page.");
            }
        }

        private void ContextOnMapRequestHandler(object sender, EventArgs eventArgs)
        {
            EventStream.AddEvent(HttpContext.Current.Request.Path + " - MapRequestHandler");
        }

        private void ContextOnLogRequest(object sender, EventArgs eventArgs)
        {
            EventStream.AddEvent(HttpContext.Current.Request.Path + " - LogRequest");
        }

        private void ContextOnBeginRequest(object sender, EventArgs e)
        {
            EventStream.AddEvent(HttpContext.Current.Request.Path + " - BeginRequest");
        }

        private void ContextOnAuthenticateRequest(object sender, EventArgs eventArgs)
        {
            EventStream.AddEvent(HttpContext.Current.Request.Path + " - AuthenticateRequest");
        }

        private void ContextOnAuthorizeRequest(object sender, EventArgs eventArgs)
        {
            EventStream.AddEvent(HttpContext.Current.Request.Path + " - AuthorizeRequest");
        }

        private void ContextOnEndRequest(object sender, EventArgs eventArgs)
        {
            EventStream.AddEvent(HttpContext.Current.Request.Path + " - EndRequest");
        }

        public void Dispose()
        {
        }
    }
}