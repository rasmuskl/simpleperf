﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace SimplePerf.Sample.Mvc3.Controllers
{
    public class VisualizeController : Controller
    {
        public ActionResult Index()
        {
            var sessions = LoadSessions();

            var model = new VisualizeModel();

            model.UtcStarted = sessions.Min(x => x.UtcStarted);
            model.UtcEnd = sessions.Max(x => x.UtcStarted);
            model.Count = sessions.Count();

            model.HourGroups = sessions
                .GroupBy(x => x.UtcStarted.ToString("dd-MM-yyyy-HH"))
                .Select(x => new GroupCount(x.Key, x.Count()))
                .ToArray();

            model.StatusGroups = sessions
                .Where(x => x.GetInformation("AspNet") as JObject != null)
                .GroupBy(x => ((JObject)x.GetInformation("AspNet"))["Status"])
                .Select(x => new GroupCount(x.Key.ToString(), x.Count()))
                .ToArray();            
            
            model.SurveyGroups = sessions
                .Where(x => x.GetInformation("ShowQuestion") as JObject != null)
                .GroupBy(x => ((JObject)x.GetInformation("ShowQuestion"))["SurveyId"])
                .Select(x => new GroupCount(x.Key.ToString(), x.Count()))
                .ToArray();

            model.ElapsedGroups = sessions
                .GroupBy(x => (x.ElaspedMilliseconds / 100) * 100)
                .OrderByDescending(x => x.Key)
                .Select(x => new GroupCount("" + x.Key, x.Count()))
                .ToArray();

            var counts = new Dictionary<string, int>();

            foreach (var session in sessions)
            {
                foreach (var pair in session.Information)
                {
                    var jObject = pair.Value as JObject;

                    foreach (var property in jObject.Properties())
                    {
                        var path = pair.Key + "/" + property.Name;

                        if (counts.ContainsKey(path))
                        {
                            counts[path] += 1;
                        } 
                        else
                        {
                            counts[path] = 1;
                        }
                    }
                }
            }

            var values = new Dictionary<string, HashSet<object>>();

            foreach (var session in sessions)
            {
                foreach (var pair in session.Information)
                {
                    var jObject = pair.Value as JObject;

                    foreach (var property in jObject.Properties())
                    {
                        var path = pair.Key + "/" + property.Name;

                        HashSet<object> set;
                        
                        if (!values.TryGetValue(path, out set))
                        {
                            set = new HashSet<object>();
                            values[path] = set;
                        }

                        switch (property.Value.Type)
                        {
                            case JTokenType.Integer:
                                set.Add(property.Value.ToObject<int>());
                                break;
                            case JTokenType.String:
                                set.Add(property.Value.ToObject<string>());
                                break;
                            case JTokenType.Guid:
                                set.Add(property.Value.ToObject<Guid>());
                                break;
                            case JTokenType.Boolean:
                                set.Add(property.Value.ToObject<bool>());
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }
            }

            model.PropertyGrouped = counts
                .Select(x => new GroupCount(x.Key, x.Value))
                .ToArray();

            model.PropertyValueGrouped = values
                .Select(x => new GroupCount(x.Key, x.Value.Count))
                .ToArray();

            return View(model);
        }

        private PerfSession[] LoadSessions()
        {
            var appDataPath = Server.MapPath("~/App_Data/");
            var appDataDirectory = new DirectoryInfo(appDataPath);
            var biggestFile = appDataDirectory.GetFiles().OrderByDescending(x => x.Length).FirstOrDefault();
            var lines = System.IO.File.ReadAllLines(biggestFile.FullName);

            var sessions = lines
                .Select(JsonConvert.DeserializeObject<PerfSession>)
                .ToArray();

            foreach (var session in sessions)
            {
                foreach (var pair in session.Information.ToArray())
                {
                    session.Information[pair.Key] = JsonConvert.DeserializeObject(pair.Value.ToString(), new JsonSerializerSettings() { Converters = new [] {new ExpandoObjectConverter()}});
                }
            }

            return sessions;
        }
    }

    public class VisualizeModel
    {
        public DateTime UtcStarted { get; set; }
        public DateTime UtcEnd { get; set; }
        public int Count { get; set; }

        public GroupCount[] HourGroups { get; set; }
        public GroupCount[] StatusGroups { get; set; }
        public GroupCount[] ElapsedGroups { get; set; }
        public GroupCount[] SurveyGroups { get; set; }
        public GroupCount[] PropertyGrouped { get; set; }

        public GroupCount[] PropertyValueGrouped { get; set; }
    }

    public class GroupCount
    {
        private readonly string _key;
        private readonly int _count;

        public string Key
        {
            get { return _key; }
        }

        public int Count
        {
            get { return _count; }
        }

        public GroupCount(string key, int count)
        {
            _key = key;
            _count = count;
        }
    }
}