﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using SimplePerf.Data;
using SimplePerf.Sample.Mvc3.Models;
using SimplePerf.Sample.Mvc3.Modules;

namespace SimplePerf.Sample.Mvc3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var dataApi = new SimplePerfDataApi(MvcApplication.PerfSessionRepository);

            var pathAggregates = dataApi.GetAggregatesBySearchPath("AspNet/Path");
            var contentAggregates = dataApi.GetAggregatesBySearchPath("AspNet/Ext");

            var contentStatusAggregates = dataApi.GetAggregatesBySearchPath(new[] {"AspNet/Ext", "AspNet/Status"});

            var model = new IndexModel(pathAggregates, contentAggregates, contentStatusAggregates);
            return View(model);
        }

        public ActionResult About()
        {
            Thread.Sleep(1000);
            return View();
        }

        public ActionResult Events()
        {
            return View(EventModule.EventStream);
        }
    }
}
