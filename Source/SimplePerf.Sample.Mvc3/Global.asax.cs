﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SimplePerf.AspNet;
using SimplePerf.Persistance;
using SimplePerf.Sample.Mvc3.Modules;

namespace SimplePerf.Sample.Mvc3
{
    public class MvcApplication : HttpApplication
    {
        public static IPerfSessionRepository PerfSessionRepository { get; private set; }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            EventModule.EventStream.AddEvent("Application_Start - Start.");

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            InitializeSimplePerf();

            EventModule.EventStream.AddEvent("Application_Start - End.");
        }

        private void InitializeSimplePerf()
        {
            var inMemoryStorage = new InMemoryCappedSessionStorage();

            var basePath = HttpContext.Current.Server.MapPath("~/App_Data/");
            var jsonPath = JsonNetSessionStorage.SuggestPath(basePath, "SimplePerf.Sample.Mvc3");
            var jsonStorage = new JsonNetSessionStorage(jsonPath);

            PerfSessionRepository = inMemoryStorage;

            var multiDecorator = new MultiSessionPersisterDecorator(inMemoryStorage, jsonStorage);
            var asyncPersister = new AsyncSessionPersisterDecorator(multiDecorator);

            var perfer = new SimplePerfer(asyncPersister, new IFetchPerfInfo[]
                    {
                        new AspNetInfoFetcher(), 
                        new WebFormsInfoFetcher(),
                    }, 
                    x => { throw x; });

            PerfModule.CurrentPerfer = perfer;
        }
    }
}