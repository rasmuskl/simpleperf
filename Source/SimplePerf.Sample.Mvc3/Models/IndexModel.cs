using SimplePerf.Data;

namespace SimplePerf.Sample.Mvc3.Models
{
    public class IndexModel
    {
        public PerfSessionAggregate[] PathAggregates { get; set; }
        public PerfSessionAggregate[] ContentAggregates { get; set; }
        public PerfSessionAggregate[] ContentStatusAggregates { get; set; }

        public IndexModel(PerfSessionAggregate[] pathAggregates, PerfSessionAggregate[] contentAggregates, PerfSessionAggregate[] contentStatusAggregates)
        {
            PathAggregates = pathAggregates;
            ContentAggregates = contentAggregates;
            ContentStatusAggregates = contentStatusAggregates;
        }
    }
}