﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;

namespace SimplePerf.AspNet
{
    public class PerfModule : IHttpModule
    {
        private const string RequestKey = "SimplePerfSession";

        public void Init(HttpApplication context)
        {
            context.BeginRequest += BeginRequest;
            context.EndRequest += EndRequest;
        }

        private void BeginRequest(object sender, EventArgs e)
        {
            if (CurrentPerfer == null)
            {
                return;
            }

            PerfSession session = CurrentPerfer.StartNewSession("Request");
            HttpContext.Current.Items[RequestKey] = session;
        }

        private void EndRequest(object sender, EventArgs e)
        {
            if (CurrentPerfer == null)
            {
                return;
            }

            var session = HttpContext.Current.Items[RequestKey] as PerfSession;

            if (session != null)
            {
                CurrentPerfer.EndSession(session);
            }
        }

        public static PerfSession CurrentRequestSession
        {
            get
            {
                return HttpContext.Current.Items[RequestKey] as PerfSession;
            }
        }

        public void Dispose()
        {
        }

        public static SimplePerfer CurrentPerfer { get; set; }
    }
}