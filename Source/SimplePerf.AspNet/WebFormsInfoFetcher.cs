﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;

namespace SimplePerf.AspNet
{
    public class WebFormsInfoFetcher : IFetchPerfInfo
    {
        public string Key
        {
            get { return "WebForms"; }
        }

        public object FetchInformation()
        {
            var page = HttpContext.Current.CurrentHandler as Page;
            return new { PostBack = page.IsPostBack, Type = page.GetType().Name };
        }

        public bool IsRelevant()
        {
            return HttpContext.Current != null
                && HttpContext.Current.CurrentHandler is Page;
        }
    }
}