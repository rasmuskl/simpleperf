﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web;

namespace SimplePerf.AspNet
{
    public class AspNetInfoFetcher : IFetchPerfInfo
    {
        public string Key
        {
            get { return "AspNet"; }
        }

        public object FetchInformation()
        {
            var request = HttpContext.Current.Request;
            var response = HttpContext.Current.Response;

            return new
                {
                    Path = request.Path,
                    Ext = Path.GetExtension(request.Path),
                    QueryString = request.QueryString.ToString(),
                    Method = request.HttpMethod,
                    Status = response.StatusCode,
                    ContentType = response.ContentType
                };
        }

        public bool IsRelevant()
        {
            return HttpContext.Current != null;
        }
    }
}