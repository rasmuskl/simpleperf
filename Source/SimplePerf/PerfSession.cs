﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;

namespace SimplePerf
{
    public class PerfSession
    {
        private readonly Stopwatch _stopwatch = Stopwatch.StartNew();
        private readonly Dictionary<string, object> _information = new Dictionary<string, object>();

        internal PerfSession(string caption) : this()
        {
            Caption = caption;
        }

        internal PerfSession()
        {
            UtcStarted = DateTime.UtcNow;
        }

        public DateTime UtcStarted { get; set; }

        public void AddInformation(string key, object information)
        {
            _information.Add(key, information);
        }

        internal void Stop()
        {
            _stopwatch.Stop();
            ElaspedMilliseconds = _stopwatch.ElapsedMilliseconds;
        }

        public string Caption { get; private set; }

        public long ElaspedMilliseconds { get; set; }

        public Dictionary<string, object> Information
        {
            get { return _information; }
        }

        public object GetInformation(string informationPath)
        {
            var pathFragments = informationPath.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries);

            if(!pathFragments.Any() || !_information.ContainsKey(pathFragments[0]))
            {
                return null;
            }

            object target = _information[pathFragments[0]];

            for (int i = 1; i < pathFragments.Count(); i++)
            {
                var nextProperty = target.GetType().GetProperty(pathFragments[i]);
                if(nextProperty != null)
                {
                    target = nextProperty.GetValue(target, null);
                }
            }

            return target;
        }
    }
}