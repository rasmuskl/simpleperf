﻿using System.Collections.Generic;

namespace SimplePerf.Persistance
{
    public interface IPerfSessionRepository
    {
        IEnumerable<PerfSession> GetSessions();
    }
}