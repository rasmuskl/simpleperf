﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SimplePerf.Persistance
{
    public class JsonNetSessionStorage : IPerfSessionPersister
    {
        private readonly FileInfo _fileInfo;

        public JsonNetSessionStorage(string fileLocation)
        {
            _fileInfo = new FileInfo(fileLocation);
        }

        public void Persist(PerfSession session)
        {
            string json = JsonConvert.SerializeObject(session);

            using(var writer = _fileInfo.AppendText())
            {
                writer.WriteLine(json);
            }
        }

        public void Persist(IEnumerable<PerfSession> sessions)
        {
            foreach (var session in sessions)
            {
                Persist(session);
            }
        }

        public static string SuggestPath(string basePath, string appName)
        {
            var timeStampedFileName = string.Format("{0}-{1}.txt", DateTime.UtcNow.ToString("yyyyMMdd-HHmmss"), appName);
            return Path.Combine(basePath, timeStampedFileName);
        }
    }
}