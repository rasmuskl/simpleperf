﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace SimplePerf.Persistance
{
    public class MultiSessionPersisterDecorator : IPerfSessionPersister
    {
        private readonly List<IPerfSessionPersister> _persisters;

        public MultiSessionPersisterDecorator(params IPerfSessionPersister[] persisters)
        {
            _persisters = persisters.ToList();
        }

        public void Persist(PerfSession session)
        {
            _persisters.ForEach(x => x.Persist(session));
        }

        public void Persist(IEnumerable<PerfSession> sessions)
        {
            _persisters.ForEach(x => x.Persist(sessions));
        }
    }
}