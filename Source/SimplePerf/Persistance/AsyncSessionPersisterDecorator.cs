﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Collections.Generic;
using System.Threading;

namespace SimplePerf.Persistance
{
    public class AsyncSessionPersisterDecorator : IPerfSessionPersister, IDisposable
    {
        private readonly IPerfSessionPersister _innerPersister;
        private readonly BlockingCollection<PerfSession> _persistQueue = new BlockingCollection<PerfSession>(new ConcurrentQueue<PerfSession>());
        private readonly Thread _thread;

        public AsyncSessionPersisterDecorator(IPerfSessionPersister innerPersister)
        {
            _innerPersister = innerPersister;
            _thread = new Thread(MovePersistedSessions);
            _thread.Start();
        }

        private void MovePersistedSessions()
        {
            foreach (var session in _persistQueue.GetConsumingEnumerable())
            {
                _innerPersister.Persist(session);
            }
        }

        public void Persist(PerfSession session)
        {
            _persistQueue.Add(session);
        }

        public void Persist(IEnumerable<PerfSession> sessions)
        {
            foreach (var session in sessions)
            {
                Persist(session);
            }
        }

        public void Dispose()
        {
            if (_thread != null)
            {
                _thread.Abort();
            }
        }
    }
}