using System.Linq;
using System.Collections.Generic;
using System;

namespace SimplePerf.Persistance
{
    public interface IPerfSessionPersister
    {
        void Persist(PerfSession session);
        void Persist(IEnumerable<PerfSession> sessions);
    }
}