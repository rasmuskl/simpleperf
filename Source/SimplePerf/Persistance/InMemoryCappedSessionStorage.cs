using System.Linq;
using System.Collections.Generic;
using System;

namespace SimplePerf.Persistance
{
    public class InMemoryCappedSessionStorage : IPerfSessionPersister, IPerfSessionRepository
    {
        private const int SessionCap = 10000;
        private const int FuzzyCapAllowance = 100;

        private readonly List<PerfSession> _sessions = new List<PerfSession>();

        public void Persist(PerfSession session)
        {
            _sessions.Add(session);
            CheckCap();
        }
    
        public void Persist(IEnumerable<PerfSession> sessions)
        {
            _sessions.AddRange(sessions);
            CheckCap();
        }

        private void CheckCap()
        {
            if (_sessions.Count > SessionCap + FuzzyCapAllowance)
            {
                int tooMany = _sessions.Count - SessionCap;
                _sessions.RemoveRange(0, tooMany);
            }
        }

        public IEnumerable<PerfSession> GetSessions()
        {
            return _sessions.ToArray();
        }
    }
}