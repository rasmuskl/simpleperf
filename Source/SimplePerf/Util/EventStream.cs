﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Collections.Generic;

namespace SimplePerf.Util
{
    public class EventStream
    {
        private readonly string _caption;
        private readonly ConcurrentQueue<KeyValuePair<DateTime, string>> _events = new ConcurrentQueue<KeyValuePair<DateTime, string>>();

        public EventStream(string caption)
        {
            _caption = caption;
            AddEvent(_caption + " - Start.");
        }

        public void AddEvent(string description)
        {
            _events.Enqueue(new KeyValuePair<DateTime, string>(DateTime.UtcNow, description));
        }

        public void EndStream()
        {
            AddEvent(string.Format("{0} - End.", _caption));
        }

        public IEnumerable<KeyValuePair<DateTime, string>> Events
        {
            get { return _events.ToArray(); }
        }
    }
}