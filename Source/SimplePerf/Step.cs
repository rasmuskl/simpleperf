﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;

namespace SimplePerf
{
    public class Step : IDisposable
    {
        private readonly Stopwatch _stopwatch = Stopwatch.StartNew();

        public void Dispose()
        {
            _stopwatch.Stop();
        }
    }
}