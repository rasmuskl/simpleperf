﻿using System;
using System.Linq;
using System.Collections.Generic;
using SimplePerf.Persistance;

namespace SimplePerf
{
    /// <summary>
    /// Main API class for SimplePerf.
    /// </summary>
    public class SimplePerfer
    {
        private readonly IPerfSessionPersister _persister;
        private readonly Action<Exception> _errorHandler;
        private readonly List<IFetchPerfInfo> _infoFetchers;

        public SimplePerfer(IPerfSessionPersister persister, IEnumerable<IFetchPerfInfo> infoFetchers, Action<Exception> errorHandler)
        {
            _persister = persister;
            _errorHandler = errorHandler;
            _infoFetchers = infoFetchers.ToList();
        }

        public PerfSession StartNewSession(string caption)
        {
            return new PerfSession(caption);
        }

        public void EndSession(PerfSession session)
        {
            _infoFetchers.ForEach(infoFetcher =>
            {
                if (!infoFetcher.IsRelevant())
                {
                    return;
                }

                session.AddInformation(infoFetcher.Key, infoFetcher.FetchInformation());
            });

            session.Stop();

            try
            {
                _persister.Persist(session);
            }
            catch (Exception exception)
            {
                _errorHandler(exception);
            }
        }
    }
}