using System.Linq;
using System.Collections.Generic;
using System;

namespace SimplePerf.Data
{
    public class PerfSessionAggregate
    {
        private readonly object _key;
        private readonly int _count;
        private readonly long _sum;
        private readonly long _average;
        private readonly long _median;

        public object Key
        {
            get { return _key; }
        }

        public int Count
        {
            get { return _count; }
        }

        public long Sum
        {
            get { return _sum; }
        }

        public long Average
        {
            get { return _average; }
        }

        public long Median
        {
            get { return _median; }
        }

        public PerfSessionAggregate(object key, int count, long sum, long average, long median)
        {
            _key = key;
            _count = count;
            _sum = sum;
            _average = average;
            _median = median;
        }
    }
}