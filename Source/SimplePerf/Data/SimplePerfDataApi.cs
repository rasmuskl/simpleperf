﻿using System;
using System.Linq;
using System.Collections.Generic;
using SimplePerf.Persistance;

namespace SimplePerf.Data
{
    public class SimplePerfDataApi
    {
        private readonly IPerfSessionRepository _repository;

        public SimplePerfDataApi(IPerfSessionRepository repository)
        {
            _repository = repository;
        }

        public PerfSessionAggregate[] GetAggregatesBySearchPath(string searchPath)
        {
            var sessions = _repository.GetSessions()
                .GroupBy(x => x.GetInformation(searchPath))
                .Where(x => x.Key != null);

            return sessions.Select(s => new PerfSessionAggregate(s.Key, s.Count(), s.Sum(x => x.ElaspedMilliseconds), (long)s.Average(x => x.ElaspedMilliseconds), s.Skip(s.Count() / 2).First().ElaspedMilliseconds)).ToArray();
        }

        public PerfSessionAggregate[] GetAggregatesBySearchPath(string[] searchPaths)
        {
            var sessions = _repository.GetSessions()
                .Where(x => searchPaths.All(p => x.GetInformation(p) != null))
                .GroupBy(x => searchPaths
                    .Aggregate(string.Empty, (s, p) => s + x.GetInformation(p).ToString() + ", ", s => s.Trim(',', ' ')));

            return sessions.Select(s => new PerfSessionAggregate(s.Key, s.Count(), s.Sum(x => x.ElaspedMilliseconds), (long)s.Average(x => x.ElaspedMilliseconds), s.Skip(s.Count() / 2).First().ElaspedMilliseconds)).ToArray();
        }
    }
}