using System.Linq;
using System.Collections.Generic;
using System;

namespace SimplePerf
{
    public interface IFetchPerfInfo
    {
        string Key { get; }
        object FetchInformation();
        bool IsRelevant();
    }
}